<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define( 'DB_NAME', 'local' );

/** MySQL database username */
define( 'DB_USER', 'root' );

/** MySQL database password */
define( 'DB_PASSWORD', 'root' );

/** MySQL hostname */
define( 'DB_HOST', 'localhost' );

/** Database Charset to use in creating database tables. */
define( 'DB_CHARSET', 'utf8' );

/** The Database Collate type. Don't change this if in doubt. */
define( 'DB_COLLATE', '' );

/**
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         '77YncG9eW871Wtp462+5J7ohbIkVmk3yOzP/NWJeMUaGHmX6SJ1Cxgien/aJQU/v2EBeV8cyUDCvB9fcYk8yAA==');
define('SECURE_AUTH_KEY',  'bTxuhG7Q43zNzcxyDjjiLfAIcs5ys0Z67NgwBS35HZTu8kZ8yOlsj2IIY0ZqCQPhCUU3QEeassPTSnMMGhg1Tg==');
define('LOGGED_IN_KEY',    'E5F6W1kP/nRWIRNnbD1eQ5ROXJi1oc9C0hiDFsr+u1lEww8oYgjVRV2U7j3M6/lEpLs358pWP1ushUYwJOgs0Q==');
define('NONCE_KEY',        'Tm40eHLeKfmCW9/gAQ194n2ldGbT/T6p1iLHhT/xE4OA11D9RXaVCXZKCzusV51w4A1ZK+YrOSKOBy+ufNKUGA==');
define('AUTH_SALT',        'I65zdH4QTQuwsnMG3qIC54O8t4GJJ8qUIQBSyHPws6wZM9n1HoVE2TE6hWAzlmyuleFAFSrfBYGKrJwfvDLnLw==');
define('SECURE_AUTH_SALT', 'khEBjH+qNtd6M7tbtv7TR0wgmNMrg5ryWYdDO2xkx2iLTJufMxlXrSa94eJwdCaPs9h+FyUh4q3x2porI64Alg==');
define('LOGGED_IN_SALT',   'yYBG2OWGUNU+TVDxWYDvh/FDOAs959LckBc2oid33Gege/pm7GJ3cYNZFpgoV/NownxuEyFkPaJgSdg3OhTJdQ==');
define('NONCE_SALT',       '+1KKhjgr/GfIYiK4iVk0hstr1oCFyl/0R03Dd+s7prGVyRv1vdz6tqmQNQ5kd/W0IsLAApJXncfIEgWE7naE5g==');

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix = 'wp_';




/* That's all, stop editing! Happy publishing. */

/** Absolute path to the WordPress directory. */
if ( ! defined( 'ABSPATH' ) ) {
	define( 'ABSPATH', dirname( __FILE__ ) . '/' );
}

/** Sets up WordPress vars and included files. */
require_once ABSPATH . 'wp-settings.php';
